﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetics
{
    public class BinaryTreeNode<T> where T : IComparable<T>
    {
        public T Data { get; private set; }

        public BinaryTreeNode<T> Left { get; private set; }

        public BinaryTreeNode<T> Right { get; private set; }

        public BinaryTreeNode(T data)
        {
            Data = data;
        }
        public BinaryTreeNode(T data, BinaryTreeNode<T> left, BinaryTreeNode<T> right)
        {
            Data = data;
            Left = left;
            Right = right;
        }

        public void AddNode(T data)
        {
            var node = new BinaryTreeNode<T>(data);
            if (node.Data.CompareTo(Data) == -1)
            {
                if (Left != null) Left.AddNode(data);
                else Left = node;

            }
            else
            {
                if (Right != null) Right.AddNode(data);
                else Right = node;
            }
        }
        public override string ToString()
        {
            return Data.ToString();
        }
    }
}
