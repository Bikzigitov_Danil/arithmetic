﻿using Arithmetics.Polynomial;
using System;
using System.Collections.Generic;

namespace Arithmetics
{
    public interface IBasicSequence<T>
    {
        T Next();
    }

    /*public interface ISeqence<T> : IBasicSequence<T>
    {
        public T Prev();
        void Reset();
    }
    class RandomPolynomialSorted : RandomPolynomial, ISeqence<Polynomial>
    {
        private int Num;
        private List<Polynomial> PolyList;
        public RandomPolynomialSorted(int num, List<Polynomial> polyList)
        {
            Num = num;
            PolyList = polyList;
        }

        public RandomPolynomial(int deg, int coeff) : base(deg,coeff)
        {
            maxDeg = deg;
            maxCoeff = coeff;
        }

        public Polynomial Prev()
        {
            return PolyList[Num - 1];
            throw new NotImplementedException();
        }

        public void Reset()
        {
            PolyList.Clear();
            throw new NotImplementedException();
        }
    }*/
    class RandomPolynomial : IBasicSequence<IntegerPolynomial>
    {
        protected int maxDeg;
        protected int maxCoeff;
        public RandomPolynomial(int deg, int coeff)
        {
            maxDeg = deg;
            maxCoeff = coeff;
        }
        public IntegerPolynomial Next()
        {
            Random rand = new Random();
            SortedDictionary<int, int> randPolCoeff = new SortedDictionary<int, int>();
            for (int i = 0; i < rand.Next(1,maxDeg); i++)
            {
                if (!randPolCoeff.ContainsKey(i))
                {
                    randPolCoeff.Add(i, rand.Next(-maxCoeff, maxCoeff));
                }
            }
            return new IntegerPolynomial(randPolCoeff);
        }
    }
    class RandomPolynomialList
    {
        public static List<IntegerPolynomial> RandomPolyList(int num, int deg, int coeff)
        {
            List<IntegerPolynomial> polyList = new List<IntegerPolynomial>();
            RandomPolynomial rp = new RandomPolynomial(deg, coeff);
            for (int i = 0; i < num; i++)
            {
                polyList.Add(rp.Next());
            }
            return polyList;
        }
    }
    class PolynomialListToString
    {
        public static void PolyListToString(List<IntegerPolynomial> polyList)
        {
            int i = 0;
            foreach (IntegerPolynomial pol in polyList)
            {
                Console.WriteLine("{0}) Pol = {1}", i, pol);
                i++;
            }
        }
    }
}
