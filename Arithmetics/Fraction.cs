﻿
namespace Arithmetics
{
    public struct Fraction
    {   
        public int a;
        public int b;
        public Fraction(int a, int b)
        {
            this.a = a;
            this.b = b;
        }
        public static Fraction operator +(Fraction x, Fraction y) => new Fraction(x.a * y.b + x.b * y.a, x.b * y.b);
        public static Fraction operator -(Fraction x, Fraction y) => new Fraction(x.a * y.b - x.b * y.a, x.b * y.b);
        public static Fraction operator -(int x, Fraction y) => new Fraction(x * y.b - y.a, y.b);
        public static Fraction operator *(Fraction x, Fraction y) => new Fraction(x.a * y.a, x.b * y.b);


        public static implicit operator Fraction(int x)
        {
            return new Fraction(x, 1);
        }

        public override string ToString()
        {
            return a + "/" + b;
        }
    }
}