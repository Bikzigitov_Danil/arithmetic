﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetics
{
    struct SternBrocotFraction : IComparable<SternBrocotFraction>, IComparable
    {
        public readonly int a;
        public readonly int b;
        public SternBrocotFraction(int a, int b)
        {
            this.a = a;
            this.b = b;
        }
        public static SternBrocotFraction operator +(SternBrocotFraction x, SternBrocotFraction y) => new SternBrocotFraction(x.a + y.a, x.b + y.b);

        public static SternBrocotFraction operator -(SternBrocotFraction x, SternBrocotFraction y) => new SternBrocotFraction(x.a * y.b - x.b * y.a, x.b * y.b);
        public override string ToString()
        {
            return a + "/" + b;
        }
        public int CompareTo(SternBrocotFraction other)
        {
            var fraction = new SternBrocotFraction(a, b) - other;
            if (fraction.a * fraction.b > 0) return -1;
            if (fraction.a * fraction.b < 0) return 1;
            return 0;
        }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
    }
}
