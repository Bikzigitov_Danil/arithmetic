﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetics
{
    class BinaryTree<T> where T : IComparable<T>
    {
        public BinaryTreeNode<T> BinaryTreeRoot { get; private set; }

        public int BinaryTreeNodesCount { get; private set; }

        public void BinartTreeNodeAdd(T data)
        {
            if (BinaryTreeRoot == null)
            {
                BinaryTreeRoot = new BinaryTreeNode<T>(data);
                BinaryTreeNodesCount = 1;
                return;
            }
            BinaryTreeRoot.AddNode(data);
            BinaryTreeNodesCount++;
        }

        public List<T> Preorder()
        {
            if (BinaryTreeRoot == null) return new List<T>();
            return Preorder(BinaryTreeRoot);
        }
        private List<T> Preorder(BinaryTreeNode<T> currentNode)
        {
            var listData = new List<T>();
            if (currentNode != null)
            {
                listData.Add(currentNode.Data);
                if (currentNode.Left != null) listData.AddRange(Preorder(currentNode.Left));
                if (currentNode.Right != null) listData.AddRange(Preorder(currentNode.Right));
            }
            return listData;
        }

        public List<T> Postorder()
        {
            if (BinaryTreeRoot == null) return new List<T>();
            return Postorder(BinaryTreeRoot);
        }
        private List<T> Postorder(BinaryTreeNode<T> currentNode)
        {
            var listData = new List<T>();
            if (currentNode != null)
            {
                if (currentNode.Left != null) listData.AddRange(Postorder(currentNode.Left));
                if (currentNode.Right != null) listData.AddRange(Postorder(currentNode.Right));
                listData.Add(currentNode.Data);
            }
            return listData;
        }
    }
}
