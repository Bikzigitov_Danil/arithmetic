﻿using Arithmetics.Polynomial;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetics.Parsers
{
    delegate IntegerPolynomial UnaryFunction(IntegerPolynomial poly);
    delegate IntegerPolynomial BinaryFunction(IntegerPolynomial poly1, IntegerPolynomial poly2);
    class UnaryOperationDictionary
    {

        private static Dictionary<string, UnaryFunction> dictionary;
        public static Dictionary<string, UnaryFunction> Dictionary
        {
            get
            {
                if (dictionary == null)
                {
                    dictionary = new Dictionary<string, UnaryFunction>();
                }
                return dictionary;
            }
        }

        private UnaryOperationDictionary()
        { }

        public static void Add(string name, UnaryFunction op) => Dictionary.Add(name, op);
    }
    class BinaryOperationDictionary
    {

        private static Dictionary<string, BinaryFunction> dictionary;
        public static Dictionary<string, BinaryFunction> Dictionary
        {
            get
            {
                if (dictionary == null)
                {
                    dictionary = new Dictionary<string, BinaryFunction>();
                }
                return dictionary;
            }
        }

        private BinaryOperationDictionary()
        { }

        public static void Add(string name, BinaryFunction op) => Dictionary.Add(name, op);
    }
    public class PolynomialCalculator
    {
        public Dictionary<string, string> PolyVars;
        public string str;

        static PolynomialCalculator()
        {
            UnaryOperationDictionary.Add("d", new UnaryFunction((IntegerPolynomial p) => p.Diff()));
            BinaryOperationDictionary.Add("+", new BinaryFunction((IntegerPolynomial p1, IntegerPolynomial p2) => p1 + p2));
            BinaryOperationDictionary.Add("-", new BinaryFunction((IntegerPolynomial p1, IntegerPolynomial p2) => p1 - p2));
            BinaryOperationDictionary.Add("*", new BinaryFunction((IntegerPolynomial p1, IntegerPolynomial p2) => p1 * p2));
            BinaryOperationDictionary.Add("e", new BinaryFunction((IntegerPolynomial p1, IntegerPolynomial p2) => IntegerPolynomial.Eval(p1, p2)));
            BinaryOperationDictionary.Add("c", new BinaryFunction((IntegerPolynomial p1, IntegerPolynomial p2) => IntegerPolynomial.Composition(p1, p2)));
        }

        public PolynomialCalculator()
        {
            str = "";
            PolyVars = new Dictionary<string, string>();
        }

        //Стек в string полином
        private static string SolveOfMathExp(string poly)
        {
            try
            {
                var HelpInputQ = ExpresssionParser.ParseOfME(poly);
                Stack<string> PolyString = new Stack<string>();
                string c;
                while (HelpInputQ.Count != 0)
                {
                    if (HelpInputQ.Peek() != "*" && HelpInputQ.Peek() != "-" && HelpInputQ.Peek() != "+" && HelpInputQ.Peek() != "^" && HelpInputQ.Peek() != "d" && HelpInputQ.Peek() != "e")
                    {
                        PolyString.Push(HelpInputQ.Dequeue());
                    }
                    else
                    {
                        c = HelpInputQ.Dequeue();
                        string helpPoly;
                        if (c == "^")
                        {
                            helpPoly = PolyString.Pop();
                            helpPoly = PolyString.Pop() + helpPoly;
                            PolyString.Push(helpPoly);
                            helpPoly = "";
                        }
                        if (UnaryOperationDictionary.Dictionary.ContainsKey(c))
                        {
                            UnaryFunction Func = UnaryOperationDictionary.Dictionary[c];
                            PolyString.Push(Func(PolynomialParser.Parse(PolyString.Pop())).ToString());
                        }
                        else if (BinaryOperationDictionary.Dictionary.ContainsKey(c))
                        {
                            BinaryFunction Func = BinaryOperationDictionary.Dictionary[c];
                            IntegerPolynomial op2 = PolynomialParser.Parse(PolyString.Pop());
                            IntegerPolynomial op1 = PolynomialParser.Parse(PolyString.Pop());
                            PolyString.Push(Func(op1, op2).ToString());
                        }
                    }
                }
                return PolynomialParser.Parse(PolyString.Pop()).ToString();
            }
            catch (NullReferenceException)
            {
                return "Error";
            }
            catch (InvalidExpressionException)
            {
                return "Error";
            }
            catch (Exception ex)
            {
                return "Error: "+ex.Message;
            }
        }

        public string IntegerPolyEquals(string poly)
        {
            try
            {
                poly = poly.Replace(" ", "");
                if (!poly.Contains(":="))
                {
                    string polyTest = poly;
                    if (PolyVars.Count > 0)
                    {
                        foreach (var eq in PolyVars) poly = poly.Replace(eq.Key, "(" + eq.Value + ")");
                        return polyTest + " = " + SolveOfMathExp(poly);
                    }
                    else return polyTest + " => " + poly + " = " + SolveOfMathExp(poly);
                }
                else
                {
                    poly = poly.Replace(":", "");
                    var splitOfEq = poly.Split('=');
                    string helpPoly;
                    if (!PolyVars.ContainsKey(splitOfEq[0])) PolyVars.Add(splitOfEq[0], "0");
                    helpPoly = splitOfEq[1];
                    if (PolyVars.Count > 0)
                    {
                        foreach (var eq in PolyVars) helpPoly = helpPoly.Replace(eq.Key, "(" + eq.Value + ")");
                    }
                    PolyVars.Remove(splitOfEq[0]);
                    PolyVars.Add(splitOfEq[0], SolveOfMathExp(helpPoly));
                    return poly + " => " + splitOfEq[0] + ":=" + PolyVars[splitOfEq[0]];
                }
            }
            catch (NullReferenceException)
            {
                return "Пусто";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        /// <summary>
        /// Обрабатывает строки DAA-способом. Возвращает null если возвращать нечего. 
        /// </summary>
        /// <param name="poly"></param>
        /// <returns></returns>
        public string DAAParser(string poly)
        {
            try
            {
                if (poly.Contains("while") || str != "")
                {
                    str += poly;
                    if (str[str.Length - 1] != '}')
                    {
                        return null;
                    }
                    poly = str;
                    str = "";
                    poly = poly.Remove(0, 5);
                    poly = poly.Replace(" ", "");
                    poly = poly.Replace("){", "&");
                    poly = poly.Remove(0, 1);
                    poly = poly.Remove(poly.Length - 1, 1);
                    var condtitonals = poly.Split('&');
                    string s = "";
                    //if (PolyVars.ContainsKey(condtitonals[0])) throw new Exception("Unknown variable present");
                    if (condtitonals[1].Contains(';'))
                    {
                        var condtitIncondit = condtitonals[1].Split(';');
                        while (SolveOfMathExp(PolyVars[condtitonals[0]]) != "0")
                        {
                            for (int i = 0; i < condtitIncondit.Length; i++)
                            {
                                s += IntegerPolyEquals(condtitIncondit[i]) + "\r\n";
                            }
                        }
                        return s;
                    }
                    else
                    {
                        while (SolveOfMathExp(PolyVars[condtitonals[0]]) != "0")
                        {
                            s += IntegerPolyEquals(condtitonals[1]) + "\r\n";
                        }
                        return s;
                    }
                }
                else return IntegerPolyEquals(poly);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
