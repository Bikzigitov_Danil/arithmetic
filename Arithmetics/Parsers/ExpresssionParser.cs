﻿using Arithmetics.Polynomial;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetics.Parsers
{
    public class ExpresssionParser
    {

        private static Dictionary<string, int> functionPriorities;

        static ExpresssionParser()
        {
            functionPriorities = new Dictionary<string, int>
            {
                { "+", 1 },
                { "-", 1 },
                { "*", 2 },
                { "^", 3 },
                { "d", 4 },
                { "e", 4 },
                { "c", 4 }
            };
        }

        /// <summary>
        /// Парсер + решатель математических выражений
        /// </summary>
        /// <param name="poly">string</param>
        /// <returns>string</returns>
        public static Queue<string> ParseOfME(string poly) //parser of math expressions
        {
            Queue<string> output = new Queue<string>();
            string helpPoly = poly;
            //if (helpPoly == "") return null;//"Empty string";
            helpPoly = helpPoly.Replace(" ", "");
            helpPoly = helpPoly.Replace("+x", "+1x");
            helpPoly = helpPoly.Replace("-x", "-1x");
            helpPoly = helpPoly.Replace("Diff", "d");
            helpPoly = helpPoly.Replace("diff", "d");
            helpPoly = helpPoly.Replace("eval", "e");
            helpPoly = helpPoly.Replace("Eval", "e");
            helpPoly = helpPoly.Replace("Comp", "c");
            helpPoly = helpPoly.Replace("comp", "c");
            helpPoly = helpPoly.Insert(helpPoly.Length, "stop");
            Stack<string> FunctionsOrOperators = new Stack<string>();
            string helpCoeff = "";
            int specialDigit = 0;
            while (helpPoly != "stop")
            {
                string polyCatch = helpPoly;
                while (char.IsDigit(helpPoly[0]))
                {
                    helpCoeff += helpPoly[0];
                    helpPoly = helpPoly.Remove(0, 1);
                    specialDigit = 1;
                }
                if (specialDigit == 1)
                {
                    if (helpPoly[0] == 'x')
                    {
                        output.Enqueue(helpCoeff + 'x');
                        helpPoly = helpPoly.Remove(0, 1);
                    }
                    else
                    {
                        output.Enqueue(helpCoeff);
                    }
                    helpCoeff = "";
                    specialDigit = 0;
                }
                if (helpPoly[0] == 'x')
                {
                    output.Enqueue(Convert.ToString(helpPoly[0]));
                    helpPoly = helpPoly.Remove(0, 1);
                }
                if (helpPoly[0] == 'd' || helpPoly[0] == 'e' || helpPoly[0] == 'c')
                {
                    FunctionsOrOperators.Push(Convert.ToString(helpPoly[0]));
                    helpPoly = helpPoly.Remove(0, 1);
                }
                if (helpPoly[0] == '+' || helpPoly[0] == '-' || helpPoly[0] == '*' || helpPoly[0] == '^')
                {
                    if (helpPoly[0] == '+' || helpPoly[0] == '-')
                    {
                        if (FunctionsOrOperators.Count != 0)
                        {
                            while (FunctionsOrOperators.Count != 0 && functionPriorities.ContainsKey(FunctionsOrOperators.Peek()) && functionPriorities[FunctionsOrOperators.Peek()] > 0)
                            {
                                if (FunctionsOrOperators.Count != 0) output.Enqueue(FunctionsOrOperators.Pop());
                            }
                        }
                    }
                    if (helpPoly[0] == '*')
                    {
                        if (FunctionsOrOperators.Count != 0)
                        {
                            while (FunctionsOrOperators.Count != 0 && functionPriorities.ContainsKey(FunctionsOrOperators.Peek()) && functionPriorities[FunctionsOrOperators.Peek()] > 1)
                            {
                                if (FunctionsOrOperators.Count != 0) output.Enqueue(FunctionsOrOperators.Pop());
                            }
                        }
                    }
                    if (helpPoly[0] == '^')
                    {
                        if (FunctionsOrOperators.Count != 0)
                        {
                            while (FunctionsOrOperators.Count != 0 && functionPriorities.ContainsKey(FunctionsOrOperators.Peek()) && functionPriorities[FunctionsOrOperators.Peek()] > 2)
                            {
                                if (FunctionsOrOperators.Count != 0) output.Enqueue(FunctionsOrOperators.Pop());
                            }
                        }
                    }
                    if (helpPoly[0] == 'd' || helpPoly[0] == 'e' || helpPoly[0] == 'c')
                    {
                        if (FunctionsOrOperators.Count != 0)
                        {
                            while (FunctionsOrOperators.Count != 0 && functionPriorities.ContainsKey(FunctionsOrOperators.Peek()) && functionPriorities[FunctionsOrOperators.Peek()] > 3)
                            {
                                if (FunctionsOrOperators.Count != 0) output.Enqueue(FunctionsOrOperators.Pop());
                            }
                        }
                    }
                    FunctionsOrOperators.Push(Convert.ToString(helpPoly[0]));
                    helpPoly = helpPoly.Remove(0, 1);
                }
                if (helpPoly[0] == '(')
                {
                    FunctionsOrOperators.Push("(");
                    helpPoly = helpPoly.Remove(0, 1);
                }
                if (helpPoly[0] == ',')
                {
                    while (FunctionsOrOperators.Peek() != "(" && FunctionsOrOperators.Count != 0)
                    {
                        output.Enqueue(FunctionsOrOperators.Pop());
                    }
                    //if (FunctionsOrOperators.Count == 0) throw new Exception("ex");
                    helpPoly = helpPoly.Remove(0, 1);
                }
                if (helpPoly[0] == ')')
                {
                    while (FunctionsOrOperators.Peek() != "(")
                    {
                        output.Enqueue(FunctionsOrOperators.Pop());
                    }
                    if (FunctionsOrOperators.Count == 0) throw new Exception("Incorrectly written polynomial");
                    if (FunctionsOrOperators.Peek() == "(") FunctionsOrOperators.Pop();
                    helpPoly = helpPoly.Remove(0, 1);
                }
                if (helpPoly == polyCatch) throw new Exception("Unknown variable present");
            }
            while (FunctionsOrOperators.Count != 0)
            {
                if (FunctionsOrOperators.Peek() == ")" || FunctionsOrOperators.Peek() == "(") throw new Exception("Incorrectly written polynomial");
                output.Enqueue(FunctionsOrOperators.Pop());
            }
            return output;
        }
    }
}
