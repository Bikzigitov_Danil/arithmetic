﻿using Arithmetics.Polynomial;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Arithmetics.Parsers
{
    /// <summary>
    /// Преобразует строку в полином
    /// </summary>
    public class PolynomialParser
    {
        /// <summary>
        /// Преобразовние строки в полином
        /// </summary>
        /// <param name="poly">Полином</param>
        /// <returns>выводит полином, класса Polynomial</returns>
        public static IntegerPolynomial Parse(string poly)
        {
            SortedDictionary<int, int> StrToPoly = new SortedDictionary<int, int>();
            string Poly = poly;
            Poly = Poly.Insert(poly.Length, "+0");
            if (Poly[0] == '-' || Poly[0] == '+') Poly = Poly.Insert(0, "0");
            else Poly = Poly.Insert(0, "0+");
            Poly = Poly.Replace("+x", "+1x");
            Poly = Poly.Replace("-x", "-1x");
            Poly = Poly.Replace("x-", "x^1-");
            Poly = Poly.Replace("x+", "x^1+");
            Poly = Poly.Replace("x^0", "");
            Poly = Poly.Replace("^", "");
            Poly = Poly.Replace("-", "+-");
            try
            {
                int coef = 0;
                int deg = 0;
                string[] monoms = Poly.Split('+');
                foreach (var mon in monoms)
                {
                    string[] parts = mon.Split('x');
                    if (parts.Length == 2)
                    {
                        coef = Convert.ToInt32(parts[0]);
                        deg = Convert.ToInt32(parts[1]);
                    }
                    if (parts.Length == 1)
                    {
                        coef = Convert.ToInt32(parts[0]);
                        deg = 0;
                    }
                    if (parts.Length > 2)
                    {
                        SortedDictionary<int, int> PolyNone = new SortedDictionary<int, int>();
                        return new IntegerPolynomial(PolyNone);
                    }
                    if (StrToPoly.ContainsKey(deg)) StrToPoly[deg] += coef;
                    else StrToPoly.Add(deg, coef);
                }
                IntegerPolynomial polynom = new IntegerPolynomial(StrToPoly);
                return polynom;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Error, Pol - 0");
                SortedDictionary<int, int> PolyNone = new SortedDictionary<int, int>();
                return new IntegerPolynomial(PolyNone);
            }
            catch (OverflowException)
            {
                Console.WriteLine("Error, Pol - 0");
                SortedDictionary<int, int> PolyNone = new SortedDictionary<int, int>();
                return new IntegerPolynomial(PolyNone);
            }
            catch (FormatException)
            {
                Console.WriteLine("Error, Pol - 0");
                SortedDictionary<int, int> PolyNone = new SortedDictionary<int, int>();
                return new IntegerPolynomial(PolyNone);
            }
        }

        public static IntegerPolynomial ArthurParse(string str)
        {
            try
            {
                SortedDictionary<int, int> arr = new SortedDictionary<int, int>();
                int key = 0, value = 0;
                string num = "";

                for (int i = 0; i < str.Length; i++)
                {
                    if (i == 0 && str[i] == '-')
                    {
                        num += str[i];
                    }

                    if (Char.IsDigit(str[i]))
                    {
                        num += str[i];
                    }
                    else
                    {
                        if (str[i] == 'x')
                        {
                            key = 1;

                            if (num == "-")
                            {
                                num += '1';
                            }

                            if (num == "")
                            {
                                value = 1;
                            }
                            else
                            {
                                value = Convert.ToInt32(num);
                            }
                            num = "";
                        }

                        if (str[i] == '+' || str[i] == '-' && i != 0)
                        {
                            if (key == 0)
                            {
                                if (arr.ContainsKey(key))
                                {
                                    arr[key] += Convert.ToInt32(num);
                                }
                                else
                                {
                                    arr.Add(key, Convert.ToInt32(num));
                                }
                            }
                            else
                            {
                                if (key == 1 && num == "")
                                {
                                    if (arr.ContainsKey(key))
                                    {
                                        arr[key] += value;
                                    }
                                    else
                                    {
                                        arr.Add(key, value);
                                    }
                                }
                                else
                                {
                                    key = Convert.ToInt32(num);

                                    if (arr.ContainsKey(key))
                                    {
                                        arr[key] += value;
                                    }
                                    else
                                    {
                                        arr.Add(key, value);
                                    }
                                }
                            }
                            num = "";
                            key = 0;
                            value = 0;
                            if (str[i] == '-') num += str[i];
                        }
                    }
                }
                if (value != 0)
                {
                    if (num.Length > 0)
                    {
                        key = Convert.ToInt32(num);
                    }
                    if (arr.ContainsKey(key))
                    {
                        arr[key] += value;
                    }
                    else
                    {
                        arr.Add(key, value);
                    }
                }
                return new IntegerPolynomial(arr);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Error, Pol - 0");
                SortedDictionary<int, int> PolyNone = new SortedDictionary<int, int>();
                return new IntegerPolynomial(PolyNone);
            }
            catch (FormatException)
            {
                Console.WriteLine("Error, Pol - 0");
                SortedDictionary<int, int> PolyNone = new SortedDictionary<int, int>();
                return new IntegerPolynomial(PolyNone);
            }
        }
    }
}
