﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arithmetics
{
    static class SternBrocotTree
    {
        public static BinaryTree<SternBrocotFraction> BuildSternBrocotTree(int level)
        {
            var first = new SternBrocotFraction(0, 1);
            var second = new SternBrocotFraction(1, 0);
            var sternBrocotTree = new BinaryTree<SternBrocotFraction>();
            Build(level, sternBrocotTree, first, second, 1);
            return sternBrocotTree;
        }
        public static void Build(int level, BinaryTree<SternBrocotFraction> sternBrocot, SternBrocotFraction first, SternBrocotFraction second, int currentLevel = 1)
        {
            if (currentLevel <= level)
            {
                var x = first + second;
                sternBrocot.BinartTreeNodeAdd(x);
                Build(level, sternBrocot, first, x, currentLevel + 1);
                Build(level, sternBrocot, x, second, currentLevel + 1);
            }
            return;
        }
    }
}
