﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Arithmetics.Polynomial
{
    class CompInv : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            if (x < y)
                return 1;
            if (x > y)
                return -1;
            else return 0;
        }
    }

    public class Polynomial<T> : IEnumerable<int>
    {
        //Скрытый массив коэффициентов полинома (Сортировка от старшей степени к свободному члену)
        public SortedDictionary<int, T> coeff = new SortedDictionary<int, T>(new CompInv());

        /// <summary>
        /// Коэффициент по степени
        /// </summary>
        /// <param name="index">на вход степень</param>
        /// <returns>на выход коэффициент</returns>
        public T this[int index] => coeff[index];
        public IEnumerator<int> GetEnumerator()
        {
            return coeff.Keys.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return coeff.Keys.GetEnumerator();
        }
    }

    /// <summary>
    /// Многочлен одной переменной
    /// </summary>
    public class IntegerPolynomial : Polynomial<int>, IComparable<IntegerPolynomial>
    {
        /*//Скрытый массив коэффициентов полинома (Сортировка от старшей степени к свободному члену)
        SortedDictionary<int, int> coeff = new SortedDictionary<int, int>(new CompInv());*/

        /// <summary>
        /// Конструктор полинома
        /// </summary>
        /// <param name="coeff">"Строит" полином по коэффициентам</param>
        public IntegerPolynomial()
        {
            coeff = new SortedDictionary<int, int>
            {
                { 0, 0 }
            };
        }

        /// <summary>
        /// Конструктор полинома
        /// </summary>
        /// <param name="coeff">"Строит" полином по коэффициентам</param>
        public IntegerPolynomial(SortedDictionary<int, int> coeff)
        {
            this.coeff = new SortedDictionary<int, int>(coeff);
        }

        /// <summary>
        /// Метод дифф. полинома
        /// </summary>
        /// <returns>строит производную полинома</returns>
        public IntegerPolynomial Diff()
        {
            SortedDictionary<int, int> diffCoeff = new SortedDictionary<int, int>();
            foreach (int deg in this)
            {
                if (deg != 0) diffCoeff.Add(deg - 1, this[deg] * deg);
            }
            return new IntegerPolynomial(diffCoeff);
        }
        /// <summary>
        /// ВЫчисление полинома в точке
        /// </summary>
        /// <param name="pol">Полином</param>
        /// <param name="x">double точка</param>
        /// <returns></returns>
        public static IntegerPolynomial Eval(IntegerPolynomial pol, IntegerPolynomial x) => Composition(pol, x);

        public static IntegerPolynomial Composition(IntegerPolynomial pol1, IntegerPolynomial pol2)
        {
            int helpDeg;
            IntegerPolynomial poly = new IntegerPolynomial();
            foreach (var deg in pol1)
            {
                if (deg != 0)
                {
                    var helpPoly = pol2;
                    helpDeg = deg - 1;
                    if (helpDeg > 0)
                    {
                        while (helpDeg != 0)
                        {
                            helpPoly *= pol2;
                            helpDeg--;
                        }
                    }
                    helpPoly *= pol1[deg];
                    poly += helpPoly;
                    helpPoly = 0;
                    helpDeg = 0;
                }
                else poly += pol1[deg];
            }
            return poly;
        }

        public static implicit operator IntegerPolynomial(int v)
        {
            SortedDictionary<int, int> k = new SortedDictionary<int, int>
            {
                { 0, v }
            };
            return new IntegerPolynomial(k);
        }

        /// <summary>
        /// Сумма полиномв
        /// </summary>
        /// <param name="pol1">Первый полином</param>
        /// <param name="pol2">Второй полином</param>
        /// <returns>Строит сумму полиномов</returns>
        public static IntegerPolynomial operator +(IntegerPolynomial pol1, IntegerPolynomial pol2)
        {
            SortedDictionary<int, int> polySum = new SortedDictionary<int, int>(pol1.coeff);
            foreach (int deg in pol2)
            {
                if (polySum.ContainsKey(deg))
                {
                    polySum[deg] += pol2[deg];
                }
                else polySum.Add(deg, pol2[deg]);
            }
            return new IntegerPolynomial(polySum);
        }
        /// <summary>
        /// Сумма полинома и числа
        /// </summary>
        /// <param name="pol1">полином</param>
        /// <param name="x">число</param>
        /// <returns>Строит сумму</returns>
        public static IntegerPolynomial operator +(IntegerPolynomial pol1, int x)
        {
            SortedDictionary<int, int> Sum = new SortedDictionary<int, int>(pol1.coeff);
            if (Sum.ContainsKey(0)) Sum[0] += x;
            else Sum.Add(0, x);
            return new IntegerPolynomial(Sum);
        }
        /// <summary>
        /// Сумма полинома и числа
        /// </summary>
        /// <param name="pol1">полином</param>
        /// <param name="x">число</param>
        /// <returns>Строит сумму</returns>
        public static IntegerPolynomial operator +(int x, IntegerPolynomial pol1) => pol1 + x;
        /// <summary>
        /// Разность полинома и числа
        /// </summary>
        /// <param name="pol1">полином</param>
        /// <param name="x">число</param>
        /// <returns>Строит разность</returns>
        public static IntegerPolynomial operator -(IntegerPolynomial pol1, int x) => pol1 + (-x);
        /// <summary>
        /// Разность полинома и числа
        /// </summary>
        /// <param name="pol1">полином</param>
        /// <param name="x">число</param>
        /// <returns>Строит разность</returns>
        public static IntegerPolynomial operator -(int x, IntegerPolynomial pol1)
        {
            return x - pol1;
        }
        /// <summary>
        /// Разность полиномов
        /// </summary>
        /// <param name="pol1">полином</param>
        /// <param name="pol2">полином</param>
        /// <returns>Строит разность</returns>
        public static IntegerPolynomial operator -(IntegerPolynomial pol1, IntegerPolynomial pol2)
        {
            SortedDictionary<int, int> subCoeff = new SortedDictionary<int, int>(pol1.coeff);
            foreach (int deg in pol2)
            {
                if (pol1.coeff.ContainsKey(deg)) subCoeff[deg] -= pol2[deg];
                else subCoeff.Add(deg, -pol2[deg]);
            }
            return new IntegerPolynomial(subCoeff);
        }
        /// <summary>
        /// Произведение полинома и числа
        /// </summary>
        /// <param name="pol1">полином</param>
        /// <param name="x">число</param>
        /// <returns>Строит произведение полинома и числа</returns>
        public static IntegerPolynomial operator *(IntegerPolynomial pol1, int x)
        {
            SortedDictionary<int, int> mulCoeff = new SortedDictionary<int, int>();
            foreach (var deg in pol1) mulCoeff[deg] = pol1[deg] * x;
            return new IntegerPolynomial(mulCoeff);
        }
        /// <summary>
        /// Произведение полинома и числа
        /// </summary>
        /// <param name="pol1">полином</param>
        /// <param name="x">число</param>
        /// <returns>Строит произведение полинома и числа</returns>
        public static IntegerPolynomial operator *(int x, IntegerPolynomial pol1) => pol1 * x;
        /// <summary>
        /// Произведение полиномов
        /// </summary>
        /// <param name="pol1">полином</param>
        /// <param name="pol2">полином</param>
        /// <returns>Строит произведение полиномов</returns>
        public static IntegerPolynomial operator *(IntegerPolynomial pol1, IntegerPolynomial pol2)
        {
            SortedDictionary<int, int> mulCoeff = new SortedDictionary<int, int>();
            foreach (var deg1 in pol1)
            {
                foreach (var deg2 in pol2)
                {
                    if (mulCoeff.ContainsKey(deg1 + deg2)) mulCoeff[deg1 + deg2] += pol1[deg1] * pol2[deg2];
                    else mulCoeff.Add(deg1 + deg2, pol1[deg1] * pol2[deg2]);
                }
            }
            return new IntegerPolynomial(mulCoeff);
        }
        /// <summary>
        /// Переопределение метода ToString() для вывода полинома
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string strPoly = "";
            //StringBuilder strPoly1 = new StringBuilder();
            foreach (var pair in coeff)
            {
                if (pair.Value > 0)
                {
                    if (pair.Value != 1) strPoly += "+" + pair.Value + "x^" + pair.Key;
                    else strPoly += "+" + "x^" + pair.Key;
                }
                if (pair.Value < 0)
                {
                    if (pair.Value != 1) strPoly += pair.Value + "x^" + pair.Key;
                    else strPoly += "-x^" + pair.Key;
                }
            }
            strPoly = strPoly.Replace("+x^0", "+1");
            strPoly = strPoly.Replace("-x^0", "-1");
            strPoly = strPoly.Replace("x^0", "");
            strPoly = strPoly.Replace("x^1+", "x+");
            strPoly = strPoly.Replace("x^1-", "x-");
            strPoly = strPoly.Replace("-1x", "-x");
            if (strPoly == "") strPoly = "0";
            if (strPoly.Last() == '+') strPoly = strPoly.Remove(strPoly.Length-1, 1);
            if (strPoly == "") strPoly = "0";
            if (strPoly[0] == '+') strPoly = strPoly.Remove(0, 1);
            if (strPoly == "") strPoly = "0";
            //if (strPoly[strPoly.Length - 1] == '1' && strPoly[strPoly.Length - 2] == '^' && strPoly[strPoly.Length - 3] == 'x') strPoly = strPoly.Remove(strPoly.Length - 2, 2);
            return strPoly;
        }

        /// <summary>
        /// Сравнение по лексикографической записи
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(IntegerPolynomial other)
        {
            var helpPoly = other - this;
            foreach (int deg in helpPoly)
            {
                if (helpPoly[deg] > 0)
                {
                    helpPoly.coeff.Clear();
                    return 1;
                }
                if (helpPoly[deg] < 0) 
                {
                    helpPoly.coeff.Clear();
                    return -1;
                }
            }
            return 0;
        }
    }
}
