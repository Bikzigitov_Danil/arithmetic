﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Arithmetics.Polynomial
{
    public class RationalPolynomial : Polynomial<Fraction>
    {
        public RationalPolynomial(SortedDictionary<int, Fraction> coeff)
        {
            foreach (var pair in coeff)
            {
                this.coeff.Add(pair.Key, pair.Value);
            }
        }
        public static RationalPolynomial operator +(RationalPolynomial pol1, RationalPolynomial pol2)
        {
            SortedDictionary<int, Fraction> Sum = new SortedDictionary<int, Fraction>(pol1.coeff);
            foreach (int deg in pol2.coeff.Keys)
            {
                if (pol1.coeff.ContainsKey(deg))
                {
                    Sum[deg] += pol2[deg];
                }
                else Sum.Add(deg, pol2[deg]);
            }
            return new RationalPolynomial(Sum);
        }

        public static RationalPolynomial operator -(RationalPolynomial pol1, RationalPolynomial pol2)
        {
            SortedDictionary<int, Fraction> Raz = new SortedDictionary<int, Fraction>(pol1.coeff);
            foreach (var pair in pol2.coeff)
            {
                if (pol1.coeff.ContainsKey(pair.Key)) Raz[pair.Key] -= pair.Value;
                else Raz.Add(pair.Key, 0 - pair.Value);
            }
            return new RationalPolynomial(Raz);
        }

        public static RationalPolynomial operator *(RationalPolynomial pol1, RationalPolynomial pol2)
        {
            SortedDictionary<int, Fraction> Proizv = new SortedDictionary<int, Fraction>();
            foreach (var pair in pol1.coeff)
            {
                foreach (var kvp in pol2.coeff)
                {
                    if (Proizv.ContainsKey(pair.Key + kvp.Key)) Proizv[pair.Key + kvp.Key] += pair.Value * kvp.Value;
                    else Proizv.Add(pair.Key + kvp.Key, pair.Value * kvp.Value);
                }
            }
            return new RationalPolynomial(Proizv);
        }

        public RationalPolynomial Diff()
        {
            SortedDictionary<int, Fraction> DiffPol = new SortedDictionary<int, Fraction>();
            foreach (var pair in coeff)
            {
                if (pair.Key != 0) DiffPol.Add(pair.Key - 1, pair.Value * pair.Key);
            }
            return new RationalPolynomial(DiffPol);
        }

    }
}
