﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Arithmetics.Polynomial;
using Arithmetics.Parsers;
using Arithmetics;
using System.Xml.Schema;

namespace AppForm
{
    public partial class Form1 : Form
    {
        private DAATabPage tabPage1;
        public SortedList<string, string> file = new SortedList<string, string>();
        
        public Dictionary<object, List<string>> inputList = new Dictionary<object, List<string>>();

        private Point _imageLocation = new Point(13, 5);
        private Point _imgHitArea = new Point(13, 2);

        Image CloseImage;

        public Form1()
        {
            InitializeComponent();
            tabPage1 = new DAATabPage("Новая вкладка");
            tabControl1.Controls.Add(tabPage1);
            tabControl1.DrawMode = TabDrawMode.OwnerDrawFixed;
            tabControl1.DrawItem += tabControl1_DrawItem;
            CloseImage = Properties.Resources.close;
            tabControl1.Padding = new Point(10, 3);

            inputList.Add(tabPage1, new List<string>());

            this.KeyPreview = true;
            button1.Visible = true;
        }

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                Image img = new Bitmap(CloseImage);
                Rectangle r = e.Bounds;
                r = this.tabControl1.GetTabRect(e.Index);
                r.Offset(2, 2);
                Brush TitleBrush = new SolidBrush(Color.Black);
                Font f = this.Font;
                string title = this.tabControl1.TabPages[e.Index].Text;

                e.Graphics.DrawString(title, f, TitleBrush, new PointF(r.X, r.Y));

                if (tabControl1.SelectedIndex >= 0)
                {
                    e.Graphics.DrawImage(img, new Point(r.X + (this.tabControl1.GetTabRect(e.Index).Width - _imageLocation.X) - 4, _imageLocation.Y-1));
                }
            }
            catch (Exception) 
            {

            }
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog1.FileName, "");
                FileInfo fileInf = new FileInfo(saveFileDialog1.FileName);
                file.Add(fileInf.Name, saveFileDialog1.FileName);
                DAATabPage view = new DAATabPage(fileInf.Name);
                inputList.Add(view, new List<string>());
                tabControl1.TabPages.Add(view);
            } 
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FileInfo fileInf = new FileInfo(openFileDialog.FileName);
                if (file.ContainsKey(fileInf.Name) != true)
                {
                    file.Add(fileInf.Name, openFileDialog.FileName);
                    RichTextBox1.Clear();
                    DAATabPage newtabPage = new DAATabPage(fileInf.Name);
                    tabControl1.TabPages.Add(newtabPage);
                    inputList.Add(newtabPage, new List<string>());
                    StreamReader f = new StreamReader(openFileDialog.FileName);
                    while (!f.EndOfStream)
                    {
                        string s = f.ReadLine();
                        string text = newtabPage.PolyCalc.DAAParser(s);
                        if (text != null) // Убрать skip!
                        {
                            newtabPage.TextBox1.AppendText(text + ";\r\n");
                        }
                        inputList[newtabPage].Add(s);
                    }
                    f.Close();
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabPage1)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(saveFileDialog1.FileName, "");
                }
                FileInfo fileInf = new FileInfo(saveFileDialog1.FileName);
                file.Add(fileInf.Name, saveFileDialog1.FileName);
                DAATabPage view = new DAATabPage(fileInf.Name);
                view.Controls.Clear();
                view.Controls.Add(tabPage1.TextBox1);
                view.TextBox1.AppendText(tabPage1.TextBox1.Text);
                tabControl1.TabPages.Add(view);
                inputList.Add(view, new List<string>());
                foreach (var item in inputList[tabPage1])
                {
                    inputList[view].Add(item);
                }
                inputList[tabPage1].Clear();
                tabControl1.TabPages.Remove(tabPage1);
            }
            StreamWriter f = new StreamWriter(file[tabControl1.SelectedTab.Text]);
            foreach (var item in inputList[tabControl1.SelectedTab])
            {
                f.WriteLine(item);
            }
            f.Close();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            File.Delete(file[tabControl1.SelectedTab.Text]);
            tabControl1.TabPages.Remove(tabControl1.SelectedTab);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab is DAATabPage)
            {
                DAATabPage page = tabControl1.SelectedTab as DAATabPage;
                for (int i = 0; i < RichTextBox1.Lines.Length; i++)
                {
                    string text = page.PolyCalc.DAAParser(RichTextBox1.Lines[i]);
                    if (text != null)
                    {
                        page.TextBox1.AppendText(text + ";\r\n");
                    }
                    inputList[tabControl1.SelectedTab].Add(RichTextBox1.Lines[i]);
                }
            }
        }

        private void tabControl1_MouseClick(object sender, MouseEventArgs e)
        {
            TabControl tc = (TabControl)sender;
            Point p = e.Location;
            int _tabWidth = tabControl1.GetTabRect(tc.SelectedIndex).Width - _imgHitArea.X;
            Rectangle r = this.tabControl1.GetTabRect(tc.SelectedIndex);
            r.Offset(_tabWidth, _imgHitArea.Y);
            r.Width = 16;
            r.Height = 16;
            if (tabControl1.SelectedTab == tabPage1 && r.Contains(p))
            {
                Form2 newForm = new Form2();
                newForm.ShowDialog();

                if (newForm.DialogResult == DialogResult.OK)
                {
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog
                    {
                        Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
                    };

                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        File.WriteAllText(saveFileDialog1.FileName, tabPage1.TextBox1.Text);
                        StreamWriter f = new StreamWriter(saveFileDialog1.FileName);
                        foreach (var item in inputList[tabControl1.SelectedTab])
                        {
                            f.WriteLine(item);
                        }
                        f.Close();
                        inputList[tabControl1.SelectedTab].Clear();
                    }
                    tabControl1.TabPages.Remove(tabControl1.SelectedTab);
                }
                else tabControl1.TabPages.Remove(tabControl1.SelectedTab);
            }
            else
            {
                if (r.Contains(p))
                {
                    if (tabControl1.SelectedTab is DAATabPage)
                    {
                        DAATabPage page = tabControl1.SelectedTab as DAATabPage;
                        File.WriteAllText(file[tabControl1.SelectedTab.Text], page.TextBox1.Text);
                        StreamWriter f = new StreamWriter(file[tabControl1.SelectedTab.Text]);
                        foreach (var item in inputList[tabControl1.SelectedTab])
                        {
                            f.WriteLine(item);
                        }
                        f.Close();
                        inputList[tabControl1.SelectedTab].Clear();
                        file.Remove(tabControl1.SelectedTab.Text);
                        tc.TabPages.Remove(tabControl1.SelectedTab);
                    }
                }
            }
        }
    }
}
