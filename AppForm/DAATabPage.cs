﻿using Arithmetics.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppForm
{
    public class DAATabPage : TabPage
    {
        public TextBox TextBox1 { get; private set; }

        public PolynomialCalculator PolyCalc { get; }
        public DAATabPage(string name) : base(name)
        {
            PolyCalc = new PolynomialCalculator();
            TextBox1 = new TextBox
            {
                Multiline = true,
                Height = 403,
                Width = 959,
                ReadOnly = true
            };
            Controls.Add(TextBox1);
        }
    }
}
